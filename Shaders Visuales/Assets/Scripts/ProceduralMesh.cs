// Script para hacer figura procedural. Recuerda agregar empty game object para hacer la figura y game objects para modificarla.
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralMesh : MonoBehaviour
{

  public Material material;
  private Mesh mesh;
  private MeshFilter meshfilter;
  private MeshRenderer meshrenderer;

  public Transform[] corners;
    // Start is called before the first frame update
    void Start()
    {
        meshrenderer=gameObject.AddComponent<MeshRenderer>();
        meshrenderer.material= material;

        mesh = new Mesh();
        mesh.name = "Mesh Procedural";


        // Para cuadrado estatico
        // mesh.vertices = new Vector3[]{
        //   new Vector3(-1,1,0), // Vertice 0
        //   new Vector3(1,1,0), // Vertice 1
        //   new Vector3(1,-1,0), // Vertice 2
        //   new Vector3(-1,-1,0) // Vertice 3
        // };

        // Para hacer Cuadrado procedural
        mesh.vertices = new Vector3[]{
           new Vector3(corners[0].position.x, corners[0].position.y, corners[0].position.z), // Vertice 0
           new Vector3(corners[1].position.x, corners[0].position.y, corners[1].position.z), // Vertice 1
           new Vector3(corners[1].position.x, corners[1].position.y, corners[1].position.z), // Vertice 2
           new Vector3(corners[0].position.x, corners[1].position.y, corners[0].position.z), // Vertice 3
           new Vector3(corners[2].position.x, corners[2].position.y, corners[2].position.z), // Vertice 4
           new Vector3(corners[3].position.x, corners[2].position.y, corners[3].position.z), // Vertice 5
           new Vector3(corners[3].position.x, corners[3].position.y, corners[3].position.z), // Vertice 6
           new Vector3(corners[2].position.x, corners[3].position.y, corners[2].position.z)  // Vertice 7
         };

        // Se establecen los colores de cada vertice.
        mesh.colors = new Color []{
          Color.red, //vertice 0
          Color.green, // vertice 1
          Color.blue, // vertice 2
          Color.white, //vertice 3
          Color.yellow, // vertice 4
          Color.cyan, //vertice 5
          Color.red, // vertice 6
          Color.black // vertice 7
        };

        // Caras de la figura.
        mesh.triangles = new int[]{
          0,1,2,  // Vertices que creamos para hacer cara de triangulo
          0,2,3,
          4,5,6,
          4,6,7,
          0,1,4,
          5,4,1,
          0,4,3,
          3,4,7,
          5,1,2,
          2,6,5,
          7,2,3,
          2,7,6
          
        };

        // Coordenadas para texturas.
        mesh.uv = new Vector2[]{
          new Vector2(0,1),
          new Vector2(1,1),
          new Vector2(1,0),
          new Vector2(0,0)
        };


        meshfilter = gameObject.AddComponent<MeshFilter>();
        meshfilter.mesh = mesh;

    }

    // Update is called once per frame
    void Update()
    {
      // Para hacer Cuadrado procedural
      mesh.vertices = new Vector3[]{
           new Vector3(corners[0].position.x, corners[0].position.y, corners[0].position.z), // Vertice 0
           new Vector3(corners[1].position.x, corners[0].position.y, corners[1].position.z), // Vertice 1
           new Vector3(corners[1].position.x, corners[1].position.y, corners[1].position.z), // Vertice 2
           new Vector3(corners[0].position.x, corners[1].position.y, corners[0].position.z), // Vertice 3
           new Vector3(corners[2].position.x, corners[2].position.y, corners[2].position.z), // Vertice 4
           new Vector3(corners[3].position.x, corners[2].position.y, corners[3].position.z), // Vertice 5
           new Vector3(corners[3].position.x, corners[3].position.y, corners[3].position.z), // Vertice 6
           new Vector3(corners[2].position.x, corners[3].position.y, corners[2].position.z) // Vertice 7
         };
    }
}
