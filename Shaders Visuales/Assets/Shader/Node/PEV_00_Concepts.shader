// Shader created with Shader Forge v1.42 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Enhanced by Antoine Guillon / Arkham Development - http://www.arkham-development.com/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.42;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:34847,y:32606,varname:node_3138,prsc:2;n:type:ShaderForge.SFN_Relay,id:9359,x:31767,y:32622,cmnt:Variables Basicas,varname:node_9359,prsc:2;n:type:ShaderForge.SFN_Vector1,id:5995,x:31694,y:32823,varname:node_5995,prsc:2,v1:0.2;n:type:ShaderForge.SFN_Vector2,id:6097,x:31694,y:32882,varname:node_6097,prsc:2,v1:0,v2:0;n:type:ShaderForge.SFN_Vector3,id:7442,x:31694,y:32978,varname:node_7442,prsc:2,v1:1,v2:0,v3:1;n:type:ShaderForge.SFN_Vector4,id:596,x:31694,y:33076,varname:node_596,prsc:2,v1:0,v2:0,v3:0,v4:0;n:type:ShaderForge.SFN_ValueProperty,id:8756,x:31841,y:32824,ptovrint:False,ptlb:valor,ptin:_valor,varname:_valor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Vector4Property,id:880,x:31841,y:33077,ptovrint:False,ptlb:vector4,ptin:_vector4,varname:_vector4,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1,v2:0.08018869,v3:0.08018869,v4:1;n:type:ShaderForge.SFN_Color,id:1718,x:31841,y:32904,ptovrint:False,ptlb:node_1718,ptin:_node_1718,varname:_node_1718,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Relay,id:3410,x:32171,y:32670,cmnt:Datos del Modelo,varname:node_3410,prsc:2;n:type:ShaderForge.SFN_VertexColor,id:368,x:32151,y:32781,varname:node_368,prsc:2;n:type:ShaderForge.SFN_ObjectPosition,id:752,x:32151,y:32908,varname:node_752,prsc:2;n:type:ShaderForge.SFN_ObjectScale,id:6185,x:32151,y:33317,varname:node_6185,prsc:2,rcp:False;n:type:ShaderForge.SFN_FragmentPosition,id:5747,x:32151,y:33034,varname:node_5747,prsc:2;n:type:ShaderForge.SFN_TexCoord,id:4648,x:32151,y:33166,varname:node_4648,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Slider,id:8911,x:31684,y:32733,ptovrint:False,ptlb:node_8911,ptin:_node_8911,varname:_node_8911,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:0.5;n:type:ShaderForge.SFN_NormalVector,id:4498,x:32151,y:33472,prsc:2,pt:False;n:type:ShaderForge.SFN_ComponentMask,id:8080,x:32084,y:34094,varname:node_8080,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1;n:type:ShaderForge.SFN_Append,id:1574,x:32262,y:34104,varname:node_1574,prsc:2|A-8080-R,B-8080-G,C-8080-B;n:type:ShaderForge.SFN_Relay,id:6838,x:32480,y:32668,cmnt:Math,varname:node_6838,prsc:2;n:type:ShaderForge.SFN_Add,id:3480,x:32454,y:32729,varname:node_3480,prsc:2;n:type:ShaderForge.SFN_Subtract,id:4769,x:32450,y:32852,varname:node_4769,prsc:2;n:type:ShaderForge.SFN_Multiply,id:3770,x:32450,y:32978,varname:node_3770,prsc:2;n:type:ShaderForge.SFN_Divide,id:2490,x:32450,y:33106,varname:node_2490,prsc:2;n:type:ShaderForge.SFN_OneMinus,id:8933,x:32450,y:33235,varname:node_8933,prsc:2;n:type:ShaderForge.SFN_Lerp,id:9810,x:33306,y:32700,varname:node_9810,prsc:2;n:type:ShaderForge.SFN_ConstantLerp,id:6732,x:33491,y:32700,varname:node_6732,prsc:2,a:0,b:1;n:type:ShaderForge.SFN_Relay,id:1437,x:33427,y:32591,cmnt:Interpolation,varname:node_1437,prsc:2;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:3199,x:33306,y:32851,varname:node_3199,prsc:2;n:type:ShaderForge.SFN_RemapRange,id:6286,x:33491,y:32851,varname:node_6286,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1;n:type:ShaderForge.SFN_InverseLerp,id:8057,x:33306,y:33019,varname:node_8057,prsc:2;n:type:ShaderForge.SFN_Relay,id:1431,x:33751,y:32590,cmnt:Light Data,varname:node_1431,prsc:2;n:type:ShaderForge.SFN_LightVector,id:6101,x:33718,y:32684,varname:node_6101,prsc:2;n:type:ShaderForge.SFN_LightPosition,id:4143,x:33718,y:32813,varname:node_4143,prsc:2;n:type:ShaderForge.SFN_LightColor,id:5021,x:33718,y:32943,varname:node_5021,prsc:2;n:type:ShaderForge.SFN_LightAttenuation,id:3737,x:33718,y:33076,varname:node_3737,prsc:2;n:type:ShaderForge.SFN_AmbientLight,id:4187,x:33718,y:33210,varname:node_4187,prsc:2;n:type:ShaderForge.SFN_Relay,id:5054,x:34104,y:32587,cmnt:Camera Data,varname:node_5054,prsc:2;n:type:ShaderForge.SFN_ViewPosition,id:416,x:34074,y:32671,varname:node_416,prsc:2;n:type:ShaderForge.SFN_Bitangent,id:1423,x:32151,y:33622,varname:node_1423,prsc:2;n:type:ShaderForge.SFN_Tangent,id:8796,x:32151,y:33756,varname:node_8796,prsc:2;n:type:ShaderForge.SFN_ViewVector,id:7120,x:34074,y:32799,varname:node_7120,prsc:2;n:type:ShaderForge.SFN_ProjectionParameters,id:6805,x:34074,y:32931,varname:node_6805,prsc:2;n:type:ShaderForge.SFN_ScreenPos,id:778,x:34074,y:33061,varname:node_778,prsc:2,sctp:2;n:type:ShaderForge.SFN_ScreenParameters,id:3479,x:34074,y:33210,varname:node_3479,prsc:2;n:type:ShaderForge.SFN_Relay,id:6254,x:32706,y:32653,cmnt:Constants,varname:node_6254,prsc:2;n:type:ShaderForge.SFN_Pi,id:2350,x:32693,y:32729,varname:node_2350,prsc:2;n:type:ShaderForge.SFN_Phi,id:976,x:32693,y:32852,varname:node_976,prsc:2;n:type:ShaderForge.SFN_Tau,id:3809,x:32693,y:32961,varname:node_3809,prsc:2;n:type:ShaderForge.SFN_E,id:9591,x:32693,y:33072,varname:node_9591,prsc:2;n:type:ShaderForge.SFN_Root2,id:3012,x:32693,y:33189,varname:node_3012,prsc:2;n:type:ShaderForge.SFN_FaceSign,id:8404,x:32151,y:33886,varname:node_8404,prsc:2,fstp:0;n:type:ShaderForge.SFN_Relay,id:3017,x:32990,y:32633,cmnt:Vector Math,varname:node_3017,prsc:2;n:type:ShaderForge.SFN_Dot,id:9319,x:32960,y:32724,varname:node_9319,prsc:2,dt:0;n:type:ShaderForge.SFN_Cross,id:379,x:32960,y:32885,varname:node_379,prsc:2;n:type:ShaderForge.SFN_Distance,id:7927,x:32960,y:33016,varname:node_7927,prsc:2;n:type:ShaderForge.SFN_Reflect,id:7716,x:32960,y:33149,varname:node_7716,prsc:2;n:type:ShaderForge.SFN_Transform,id:6450,x:32960,y:33284,varname:node_6450,prsc:2,tffrom:0,tfto:1;n:type:ShaderForge.SFN_Length,id:2701,x:32960,y:33455,varname:node_2701,prsc:2;n:type:ShaderForge.SFN_Normalize,id:3514,x:32960,y:33589,varname:node_3514,prsc:2;n:type:ShaderForge.SFN_Clamp,id:954,x:32447,y:33370,varname:node_954,prsc:2;n:type:ShaderForge.SFN_Clamp01,id:8005,x:32447,y:33501,varname:node_8005,prsc:2;n:type:ShaderForge.SFN_ConstantClamp,id:605,x:32447,y:33629,varname:node_605,prsc:2,min:-4.5,max:150;n:type:ShaderForge.SFN_Floor,id:5665,x:32447,y:33778,varname:node_5665,prsc:2;n:type:ShaderForge.SFN_Round,id:8548,x:32447,y:33905,varname:node_8548,prsc:2;n:type:ShaderForge.SFN_Ceil,id:5511,x:32447,y:34036,varname:node_5511,prsc:2;n:type:ShaderForge.SFN_Relay,id:6201,x:34470,y:32583,cmnt:Logic,varname:node_6201,prsc:2;n:type:ShaderForge.SFN_If,id:583,x:34440,y:32696,varname:node_583,prsc:2|A-3695-OUT,B-649-OUT,GT-5025-OUT,EQ-8540-OUT,LT-8179-OUT;n:type:ShaderForge.SFN_Step,id:6333,x:34440,y:32837,varname:node_6333,prsc:2;n:type:ShaderForge.SFN_Vector1,id:3695,x:34257,y:32596,varname:node_3695,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:649,x:34257,y:32657,varname:node_649,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Vector3,id:5025,x:34240,y:32724,varname:node_5025,prsc:2,v1:1,v2:0,v3:0;n:type:ShaderForge.SFN_Vector3,id:8540,x:34240,y:32812,varname:node_8540,prsc:2,v1:0,v2:1,v3:0;n:type:ShaderForge.SFN_Vector3,id:8179,x:34240,y:32897,varname:node_8179,prsc:2,v1:0,v2:0,v3:1;n:type:ShaderForge.SFN_SwitchProperty,id:1283,x:34440,y:32999,ptovrint:False,ptlb:Complex,ptin:_Complex,varname:node_1283,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-5025-OUT,B-8540-OUT;n:type:ShaderForge.SFN_ToggleProperty,id:2240,x:34440,y:33284,ptovrint:False,ptlb:ToggleUno,ptin:_ToggleUno,varname:node_2240,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True;n:type:ShaderForge.SFN_Lerp,id:4629,x:34440,y:33121,varname:node_4629,prsc:2|A-5025-OUT,B-8540-OUT,T-2240-OUT;n:type:ShaderForge.SFN_Get,id:5319,x:34607,y:33378,varname:node_5319,prsc:2|IN-9735-OUT;n:type:ShaderForge.SFN_Set,id:9735,x:34456,y:33376,varname:OutputFinal,prsc:2|IN-7830-OUT;n:type:ShaderForge.SFN_Vector3,id:7830,x:34285,y:33376,varname:node_7830,prsc:2,v1:1,v2:1,v3:0;n:type:ShaderForge.SFN_Code,id:2185,x:34363,y:33510,varname:node_2185,prsc:2,code:cgBlAHQAdQByAG4AIABBACoAQgA7AA==,output:2,fname:MyMultiply,width:292,height:112,input:2,input:2,input_1_label:A,input_2_label:B|A-3164-OUT,B-1640-OUT;n:type:ShaderForge.SFN_Vector3,id:3164,x:34176,y:33507,varname:node_3164,prsc:2,v1:0.2,v2:0.2,v3:0.2;n:type:ShaderForge.SFN_Vector3,id:1640,x:34176,y:33601,varname:node_1640,prsc:2,v1:1,v2:0,v3:0;n:type:ShaderForge.SFN_Multiply,id:1086,x:34471,y:33647,varname:node_1086,prsc:2;proporder:8911-1283-2240;pass:END;sub:END;*/

Shader "Uniat/PEV_00_Concepts" {
    Properties {
        _node_8911 ("node_8911", Range(0, 0.5)) = 0.5
        [MaterialToggle] _Complex ("Complex", Float ) = 1
        [MaterialToggle] _ToggleUno ("ToggleUno", Float ) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #ifndef UNITY_PASS_FORWARDBASE
            #define UNITY_PASS_FORWARDBASE
            #endif //UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal d3d11_9x xboxone ps4 psp2 n3ds wiiu switch vulkan 
            #pragma target 3.0
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
                float3 finalColor = 0;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
