// Shader created with Shader Forge v1.42 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Enhanced by Antoine Guillon / Arkham Development - http://www.arkham-development.com/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.42;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:1073,x:33063,y:32610,varname:node_1073,prsc:2|normal-1179-RGB,emission-5703-OUT,custl-6957-OUT;n:type:ShaderForge.SFN_NormalVector,id:8849,x:32216,y:32613,prsc:2,pt:False;n:type:ShaderForge.SFN_Dot,id:9545,x:32467,y:32722,varname:node_9545,prsc:2,dt:1|A-8849-OUT,B-2780-OUT;n:type:ShaderForge.SFN_LightVector,id:2780,x:32193,y:32817,varname:node_2780,prsc:2;n:type:ShaderForge.SFN_Set,id:7490,x:32659,y:32750,varname:Lightdirection,prsc:2|IN-9545-OUT;n:type:ShaderForge.SFN_LightAttenuation,id:8999,x:32092,y:33009,varname:node_8999,prsc:2;n:type:ShaderForge.SFN_LightColor,id:1469,x:32092,y:33158,varname:node_1469,prsc:2;n:type:ShaderForge.SFN_Multiply,id:3469,x:32296,y:33047,varname:node_3469,prsc:2|A-8999-OUT,B-1469-RGB;n:type:ShaderForge.SFN_Set,id:1208,x:32487,y:33047,varname:lightcolor,prsc:2|IN-3469-OUT;n:type:ShaderForge.SFN_Multiply,id:9290,x:32552,y:33287,varname:node_9290,prsc:2|A-6944-OUT,B-5922-OUT;n:type:ShaderForge.SFN_Get,id:6944,x:32300,y:33312,varname:node_6944,prsc:2|IN-1208-OUT;n:type:ShaderForge.SFN_Get,id:5922,x:32300,y:33346,varname:node_5922,prsc:2|IN-7490-OUT;n:type:ShaderForge.SFN_Set,id:976,x:32733,y:33323,varname:lightdata,prsc:2|IN-9290-OUT;n:type:ShaderForge.SFN_Tex2d,id:6494,x:31917,y:33338,ptovrint:False,ptlb:DiffuseTexture,ptin:_DiffuseTexture,varname:node_6494,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:46e9451bf9153ea9395724b364aad6b8,ntxv:0,isnm:False|UVIN-7258-OUT;n:type:ShaderForge.SFN_Multiply,id:6759,x:32214,y:33471,varname:node_6759,prsc:2|A-6494-RGB,B-7364-RGB;n:type:ShaderForge.SFN_Color,id:7364,x:31942,y:33629,ptovrint:False,ptlb:DiffuseColor,ptin:_DiffuseColor,varname:node_7364,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.40547,c2:0.4622642,c3:0.3750445,c4:1;n:type:ShaderForge.SFN_Set,id:7957,x:32849,y:33519,varname:diffusedata,prsc:2|IN-7816-OUT;n:type:ShaderForge.SFN_AmbientLight,id:101,x:31947,y:33938,varname:node_101,prsc:2;n:type:ShaderForge.SFN_Multiply,id:2276,x:32161,y:33977,varname:node_2276,prsc:2|A-101-RGB,B-8460-OUT;n:type:ShaderForge.SFN_Get,id:8460,x:31948,y:34100,varname:node_8460,prsc:2|IN-7957-OUT;n:type:ShaderForge.SFN_Set,id:2595,x:32323,y:34013,varname:ambientdata,prsc:2|IN-2276-OUT;n:type:ShaderForge.SFN_Multiply,id:5359,x:32161,y:34160,varname:node_5359,prsc:2|A-1088-OUT,B-5479-RGB,C-6300-RGB;n:type:ShaderForge.SFN_Tex2d,id:5479,x:31844,y:34292,ptovrint:False,ptlb:emmisivetexture,ptin:_emmisivetexture,varname:node_5479,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:0bc4e079feed41033ae4cf268aa146db,ntxv:0,isnm:False|UVIN-7258-OUT;n:type:ShaderForge.SFN_Set,id:984,x:32690,y:34227,varname:emissivedata,prsc:2|IN-8051-OUT;n:type:ShaderForge.SFN_Get,id:4959,x:32672,y:33808,varname:node_4959,prsc:2|IN-976-OUT;n:type:ShaderForge.SFN_Multiply,id:672,x:32848,y:33808,varname:node_672,prsc:2|A-4959-OUT,B-6961-OUT;n:type:ShaderForge.SFN_Get,id:6961,x:32670,y:33864,varname:node_6961,prsc:2|IN-7957-OUT;n:type:ShaderForge.SFN_Set,id:8114,x:33007,y:33829,varname:diffuseshader,prsc:2|IN-672-OUT;n:type:ShaderForge.SFN_Add,id:2200,x:32139,y:35540,varname:node_2200,prsc:2|A-4432-OUT,B-1113-OUT;n:type:ShaderForge.SFN_Get,id:4432,x:31929,y:35507,varname:node_4432,prsc:2|IN-9148-OUT;n:type:ShaderForge.SFN_Get,id:1113,x:31956,y:35596,varname:node_1113,prsc:2|IN-8114-OUT;n:type:ShaderForge.SFN_Set,id:5910,x:32335,y:35540,varname:specularshader,prsc:2|IN-2200-OUT;n:type:ShaderForge.SFN_Get,id:5703,x:32828,y:32648,varname:node_5703,prsc:2|IN-984-OUT;n:type:ShaderForge.SFN_Tex2d,id:1179,x:32803,y:32351,ptovrint:False,ptlb:normaltexture,ptin:_normaltexture,varname:node_1179,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:87c2483b385219e22ac3f5bab874b750,ntxv:3,isnm:True;n:type:ShaderForge.SFN_NormalVector,id:6445,x:31905,y:34535,prsc:2,pt:False;n:type:ShaderForge.SFN_Fresnel,id:6642,x:32229,y:34614,varname:node_6642,prsc:2|NRM-6445-OUT,EXP-2971-OUT;n:type:ShaderForge.SFN_Slider,id:2971,x:31827,y:34714,ptovrint:False,ptlb:fresnelpower,ptin:_fresnelpower,varname:node_2971,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1.865249,max:10;n:type:ShaderForge.SFN_Multiply,id:8622,x:32466,y:34593,varname:node_8622,prsc:2|A-6642-OUT,B-2745-OUT,C-2964-RGB;n:type:ShaderForge.SFN_Slider,id:2745,x:32068,y:34785,ptovrint:False,ptlb:fresnelintensity,ptin:_fresnelintensity,varname:node_2745,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.9986485,max:2;n:type:ShaderForge.SFN_Color,id:2964,x:32253,y:34893,ptovrint:False,ptlb:fresnelcolor,ptin:_fresnelcolor,varname:node_2964,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1214845,c2:0.538904,c3:0.6132076,c4:1;n:type:ShaderForge.SFN_Multiply,id:4991,x:32661,y:34603,varname:node_4991,prsc:2|A-8622-OUT,B-1644-OUT;n:type:ShaderForge.SFN_Get,id:1644,x:32498,y:34763,varname:node_1644,prsc:2|IN-976-OUT;n:type:ShaderForge.SFN_Set,id:4522,x:32837,y:34719,varname:fresnellight,prsc:2|IN-4991-OUT;n:type:ShaderForge.SFN_Get,id:6957,x:32852,y:32835,varname:node_6957,prsc:2|IN-3576-OUT;n:type:ShaderForge.SFN_NormalVector,id:9978,x:31792,y:35371,prsc:2,pt:False;n:type:ShaderForge.SFN_HalfVector,id:4249,x:31806,y:35223,varname:node_4249,prsc:2;n:type:ShaderForge.SFN_Dot,id:3670,x:32014,y:35264,varname:node_3670,prsc:2,dt:1|A-4249-OUT,B-9978-OUT;n:type:ShaderForge.SFN_Slider,id:8415,x:31633,y:35096,ptovrint:False,ptlb:Specularsize,ptin:_Specularsize,varname:node_8415,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:3.940988,max:10;n:type:ShaderForge.SFN_Exp,id:3861,x:31988,y:35099,varname:node_3861,prsc:2,et:0|IN-8415-OUT;n:type:ShaderForge.SFN_Power,id:2660,x:32197,y:35229,varname:node_2660,prsc:2|VAL-3670-OUT,EXP-3861-OUT;n:type:ShaderForge.SFN_Multiply,id:1162,x:32513,y:35273,varname:node_1162,prsc:2|A-2660-OUT,B-9331-OUT,C-4076-RGB;n:type:ShaderForge.SFN_Slider,id:9331,x:32197,y:35147,ptovrint:False,ptlb:specularintensity,ptin:_specularintensity,varname:node_9331,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.7444098,max:2;n:type:ShaderForge.SFN_Color,id:4076,x:32335,y:35304,ptovrint:False,ptlb:specularcolor,ptin:_specularcolor,varname:node_4076,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:2637,x:32710,y:35273,varname:node_2637,prsc:2|A-1162-OUT,B-9813-OUT,C-2806-RGB;n:type:ShaderForge.SFN_Get,id:9813,x:32537,y:35434,varname:node_9813,prsc:2|IN-976-OUT;n:type:ShaderForge.SFN_Set,id:9148,x:32896,y:35324,varname:speculardata,prsc:2|IN-2637-OUT;n:type:ShaderForge.SFN_Color,id:6300,x:32015,y:34383,ptovrint:False,ptlb:emissivecolor,ptin:_emissivecolor,varname:node_6300,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.9645879,c2:1,c3:0.3301887,c4:1;n:type:ShaderForge.SFN_Slider,id:1088,x:31747,y:34188,ptovrint:False,ptlb:emissiveintensity,ptin:_emissiveintensity,varname:node_1088,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Get,id:608,x:32713,y:35472,varname:node_608,prsc:2|IN-5910-OUT;n:type:ShaderForge.SFN_Get,id:435,x:32679,y:35532,varname:node_435,prsc:2|IN-4522-OUT;n:type:ShaderForge.SFN_Set,id:3576,x:33080,y:35513,varname:finalshader,prsc:2|IN-9751-OUT;n:type:ShaderForge.SFN_Add,id:9751,x:32896,y:35477,varname:node_9751,prsc:2|A-608-OUT,B-435-OUT;n:type:ShaderForge.SFN_Tex2d,id:6670,x:32630,y:32907,ptovrint:False,ptlb:cloudtexture,ptin:_cloudtexture,varname:node_6670,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:1f2afde9c17703e14a776f636a4df20d,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:3001,x:32852,y:32967,varname:node_3001,prsc:2|A-9531-OUT,B-6670-B;n:type:ShaderForge.SFN_Slider,id:9531,x:32585,y:33142,ptovrint:False,ptlb:Cloudintensity,ptin:_Cloudintensity,varname:node_9531,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Tex2d,id:6330,x:32162,y:33629,ptovrint:False,ptlb:cllodtexture,ptin:_cllodtexture,varname:node_6330,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:1f2afde9c17703e14a776f636a4df20d,ntxv:0,isnm:False|UVIN-7605-OUT;n:type:ShaderForge.SFN_Add,id:7816,x:32623,y:33458,varname:node_7816,prsc:2|A-6759-OUT,B-5973-OUT;n:type:ShaderForge.SFN_Color,id:6589,x:32225,y:33823,ptovrint:False,ptlb:Cloudcolor,ptin:_Cloudcolor,varname:node_6589,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.8539516,c2:0.8937396,c3:0.8962264,c4:0.9098039;n:type:ShaderForge.SFN_Get,id:3520,x:32190,y:34478,varname:node_3520,prsc:2|IN-7490-OUT;n:type:ShaderForge.SFN_Lerp,id:8051,x:32531,y:34160,varname:node_8051,prsc:2|A-5359-OUT,B-2705-OUT,T-3520-OUT;n:type:ShaderForge.SFN_Get,id:2705,x:32161,y:34274,varname:node_2705,prsc:2|IN-7957-OUT;n:type:ShaderForge.SFN_TexCoord,id:9983,x:31431,y:33648,varname:node_9983,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Add,id:7605,x:31641,y:33478,varname:node_7605,prsc:2|A-4039-OUT,B-9983-UVOUT;n:type:ShaderForge.SFN_Time,id:9447,x:31101,y:33622,varname:node_9447,prsc:2;n:type:ShaderForge.SFN_Slider,id:2458,x:30817,y:33409,ptovrint:False,ptlb:VelocidadNubesU,ptin:_VelocidadNubesU,varname:node_2458,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.01,cur:-0.002856268,max:-0.01;n:type:ShaderForge.SFN_TexCoord,id:5765,x:31493,y:33294,varname:node_5765,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Multiply,id:940,x:31437,y:33111,varname:node_940,prsc:2|A-4131-OUT,B-4748-T;n:type:ShaderForge.SFN_Time,id:4748,x:31163,y:33268,varname:node_4748,prsc:2;n:type:ShaderForge.SFN_Slider,id:5748,x:30930,y:33056,ptovrint:False,ptlb:VelocidadtierraU,ptin:_VelocidadtierraU,varname:_VelocidadNubes_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.02,cur:0.008047668,max:-0.02;n:type:ShaderForge.SFN_Add,id:7258,x:31688,y:33189,varname:node_7258,prsc:2|A-940-OUT,B-5765-UVOUT;n:type:ShaderForge.SFN_Append,id:4131,x:31244,y:33100,varname:node_4131,prsc:2|A-5748-OUT,B-8805-OUT;n:type:ShaderForge.SFN_Slider,id:8805,x:30856,y:33173,ptovrint:False,ptlb:velocidadtierrav,ptin:_velocidadtierrav,varname:node_8805,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:0.01;n:type:ShaderForge.SFN_Append,id:386,x:31101,y:33467,varname:node_386,prsc:2|A-2458-OUT,B-8289-OUT;n:type:ShaderForge.SFN_Slider,id:8289,x:30761,y:33508,ptovrint:False,ptlb:velocidadnubesV,ptin:_velocidadnubesV,varname:node_8289,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:0.01;n:type:ShaderForge.SFN_Multiply,id:5973,x:32429,y:33499,varname:node_5973,prsc:2|A-6189-OUT,B-6612-OUT;n:type:ShaderForge.SFN_Slider,id:6612,x:32494,y:33672,ptovrint:False,ptlb:Cloudintensity,ptin:_Cloudintensity,varname:node_6612,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.09470673,max:1;n:type:ShaderForge.SFN_Tex2d,id:2806,x:32549,y:35002,ptovrint:False,ptlb:Specularmap,ptin:_Specularmap,varname:node_2806,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:87aaae85c24ef9eca84209e4ae6ccb3d,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:4039,x:31333,y:33512,varname:node_4039,prsc:2|A-386-OUT,B-9447-T;n:type:ShaderForge.SFN_Add,id:6189,x:32333,y:33644,varname:node_6189,prsc:2|A-6330-RGB,B-6589-RGB;proporder:6494-7364-5479-1179-8415-9331-4076-6300-1088-2971-2745-2964-6670-9531-6330-6589-2458-5748-8805-8289-6612-2806;pass:END;sub:END;*/

Shader "Unlit/Tierra" {
    Properties {
        _DiffuseTexture ("DiffuseTexture", 2D) = "white" {}
        _DiffuseColor ("DiffuseColor", Color) = (0.40547,0.4622642,0.3750445,1)
        _emmisivetexture ("emmisivetexture", 2D) = "white" {}
        _normaltexture ("normaltexture", 2D) = "bump" {}
        _Specularsize ("Specularsize", Range(0, 10)) = 3.940988
        _specularintensity ("specularintensity", Range(0, 2)) = 0.7444098
        _specularcolor ("specularcolor", Color) = (0.5,0.5,0.5,1)
        _emissivecolor ("emissivecolor", Color) = (0.9645879,1,0.3301887,1)
        _emissiveintensity ("emissiveintensity", Range(0, 1)) = 1
        _fresnelpower ("fresnelpower", Range(0, 10)) = 1.865249
        _fresnelintensity ("fresnelintensity", Range(0, 2)) = 0.9986485
        _fresnelcolor ("fresnelcolor", Color) = (0.1214845,0.538904,0.6132076,1)
        _cloudtexture ("cloudtexture", 2D) = "white" {}
        _Cloudintensity ("Cloudintensity", Range(0, 1)) = 1
        _cllodtexture ("cllodtexture", 2D) = "white" {}
        _Cloudcolor ("Cloudcolor", Color) = (0.8539516,0.8937396,0.8962264,0.9098039)
        _VelocidadNubesU ("VelocidadNubesU", Range(0.01, -0.01)) = -0.002856268
        _VelocidadtierraU ("VelocidadtierraU", Range(0.02, -0.02)) = 0.008047668
        _velocidadtierrav ("velocidadtierrav", Range(0, 0.01)) = 0
        _velocidadnubesV ("velocidadnubesV", Range(0, 0.01)) = 0
        _Cloudintensity ("Cloudintensity", Range(0, 1)) = 0.09470673
        _Specularmap ("Specularmap", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 100
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #ifndef UNITY_PASS_FORWARDBASE
            #define UNITY_PASS_FORWARDBASE
            #endif //UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles vulkan 
            #pragma target 3.0
            uniform sampler2D _DiffuseTexture; uniform float4 _DiffuseTexture_ST;
            uniform float4 _DiffuseColor;
            uniform sampler2D _emmisivetexture; uniform float4 _emmisivetexture_ST;
            uniform sampler2D _normaltexture; uniform float4 _normaltexture_ST;
            uniform float _fresnelpower;
            uniform float _fresnelintensity;
            uniform float4 _fresnelcolor;
            uniform float _Specularsize;
            uniform float _specularintensity;
            uniform float4 _specularcolor;
            uniform float4 _emissivecolor;
            uniform float _emissiveintensity;
            uniform sampler2D _cllodtexture; uniform float4 _cllodtexture_ST;
            uniform float4 _Cloudcolor;
            uniform float _VelocidadNubesU;
            uniform float _VelocidadtierraU;
            uniform float _velocidadtierrav;
            uniform float _velocidadnubesV;
            uniform float _Cloudintensity;
            uniform sampler2D _Specularmap; uniform float4 _Specularmap_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                UNITY_LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                UNITY_TRANSFER_LIGHTING(o, float2(0,0));
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _normaltexture_var = UnpackNormal(tex2D(_normaltexture,TRANSFORM_TEX(i.uv0, _normaltexture)));
                float3 normalLocal = _normaltexture_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                UNITY_LIGHT_ATTENUATION(attenuation, i, i.posWorld.xyz);
////// Emissive:
                float4 node_4748 = _Time;
                float2 node_7258 = ((float2(_VelocidadtierraU,_velocidadtierrav)*node_4748.g)+i.uv0);
                float4 _emmisivetexture_var = tex2D(_emmisivetexture,TRANSFORM_TEX(node_7258, _emmisivetexture));
                float4 _DiffuseTexture_var = tex2D(_DiffuseTexture,TRANSFORM_TEX(node_7258, _DiffuseTexture));
                float4 node_9447 = _Time;
                float2 node_7605 = ((float2(_VelocidadNubesU,_velocidadnubesV)*node_9447.g)+i.uv0);
                float4 _cllodtexture_var = tex2D(_cllodtexture,TRANSFORM_TEX(node_7605, _cllodtexture));
                float3 diffusedata = ((_DiffuseTexture_var.rgb*_DiffuseColor.rgb)+((_cllodtexture_var.rgb+_Cloudcolor.rgb)*_Cloudintensity));
                float Lightdirection = max(0,dot(i.normalDir,lightDirection));
                float3 emissivedata = lerp((_emissiveintensity*_emmisivetexture_var.rgb*_emissivecolor.rgb),diffusedata,Lightdirection);
                float3 emissive = emissivedata;
                float3 lightcolor = (attenuation*_LightColor0.rgb);
                float3 lightdata = (lightcolor*Lightdirection);
                float4 _Specularmap_var = tex2D(_Specularmap,TRANSFORM_TEX(i.uv0, _Specularmap));
                float3 speculardata = ((pow(max(0,dot(halfDirection,i.normalDir)),exp(_Specularsize))*_specularintensity*_specularcolor.rgb)*lightdata*_Specularmap_var.rgb);
                float3 diffuseshader = (lightdata*diffusedata);
                float3 specularshader = (speculardata+diffuseshader);
                float3 fresnellight = ((pow(1.0-max(0,dot(i.normalDir, viewDirection)),_fresnelpower)*_fresnelintensity*_fresnelcolor.rgb)*lightdata);
                float3 finalshader = (specularshader+fresnellight);
                float3 finalColor = emissive + finalshader;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #ifndef UNITY_PASS_FORWARDADD
            #define UNITY_PASS_FORWARDADD
            #endif //UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles vulkan 
            #pragma target 3.0
            uniform sampler2D _DiffuseTexture; uniform float4 _DiffuseTexture_ST;
            uniform float4 _DiffuseColor;
            uniform sampler2D _emmisivetexture; uniform float4 _emmisivetexture_ST;
            uniform sampler2D _normaltexture; uniform float4 _normaltexture_ST;
            uniform float _fresnelpower;
            uniform float _fresnelintensity;
            uniform float4 _fresnelcolor;
            uniform float _Specularsize;
            uniform float _specularintensity;
            uniform float4 _specularcolor;
            uniform float4 _emissivecolor;
            uniform float _emissiveintensity;
            uniform sampler2D _cllodtexture; uniform float4 _cllodtexture_ST;
            uniform float4 _Cloudcolor;
            uniform float _VelocidadNubesU;
            uniform float _VelocidadtierraU;
            uniform float _velocidadtierrav;
            uniform float _velocidadnubesV;
            uniform float _Cloudintensity;
            uniform sampler2D _Specularmap; uniform float4 _Specularmap_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                UNITY_LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                UNITY_TRANSFER_LIGHTING(o, float2(0,0));
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _normaltexture_var = UnpackNormal(tex2D(_normaltexture,TRANSFORM_TEX(i.uv0, _normaltexture)));
                float3 normalLocal = _normaltexture_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                UNITY_LIGHT_ATTENUATION(attenuation, i, i.posWorld.xyz);
                float3 lightcolor = (attenuation*_LightColor0.rgb);
                float Lightdirection = max(0,dot(i.normalDir,lightDirection));
                float3 lightdata = (lightcolor*Lightdirection);
                float4 _Specularmap_var = tex2D(_Specularmap,TRANSFORM_TEX(i.uv0, _Specularmap));
                float3 speculardata = ((pow(max(0,dot(halfDirection,i.normalDir)),exp(_Specularsize))*_specularintensity*_specularcolor.rgb)*lightdata*_Specularmap_var.rgb);
                float4 node_4748 = _Time;
                float2 node_7258 = ((float2(_VelocidadtierraU,_velocidadtierrav)*node_4748.g)+i.uv0);
                float4 _DiffuseTexture_var = tex2D(_DiffuseTexture,TRANSFORM_TEX(node_7258, _DiffuseTexture));
                float4 node_9447 = _Time;
                float2 node_7605 = ((float2(_VelocidadNubesU,_velocidadnubesV)*node_9447.g)+i.uv0);
                float4 _cllodtexture_var = tex2D(_cllodtexture,TRANSFORM_TEX(node_7605, _cllodtexture));
                float3 diffusedata = ((_DiffuseTexture_var.rgb*_DiffuseColor.rgb)+((_cllodtexture_var.rgb+_Cloudcolor.rgb)*_Cloudintensity));
                float3 diffuseshader = (lightdata*diffusedata);
                float3 specularshader = (speculardata+diffuseshader);
                float3 fresnellight = ((pow(1.0-max(0,dot(i.normalDir, viewDirection)),_fresnelpower)*_fresnelintensity*_fresnelcolor.rgb)*lightdata);
                float3 finalshader = (specularshader+fresnellight);
                float3 finalColor = finalshader;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
