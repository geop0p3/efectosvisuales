// Shader created with Shader Forge v1.42 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Enhanced by Antoine Guillon / Arkham Development - http://www.arkham-development.com/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.42;sub:START;pass:START;ps:flbk:Standard,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:True,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:False,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:True,fnsp:True,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:1321,x:32824,y:32695,varname:node_1321,prsc:2|normal-7211-OUT,emission-7984-OUT,custl-7700-OUT,disp-241-OUT,tess-9550-OUT;n:type:ShaderForge.SFN_Tex2d,id:5601,x:28143,y:31928,varname:node_4000,prsc:2,tex:bfe0b7cbdb6e54a4fb69c7f5b8fbcf81,ntxv:0,isnm:False|TEX-88-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:88,x:27970,y:31928,ptovrint:False,ptlb:Diffuse,ptin:_Diffuse,varname:node_3284,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:bfe0b7cbdb6e54a4fb69c7f5b8fbcf81,ntxv:0,isnm:False;n:type:ShaderForge.SFN_ValueProperty,id:9550,x:32334,y:33287,ptovrint:False,ptlb:Tesselation,ptin:_Tesselation,varname:node_4875,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Tex2d,id:3977,x:32361,y:32813,varname:node_5291,prsc:2,ntxv:0,isnm:False|TEX-9492-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:9492,x:32177,y:32834,ptovrint:False,ptlb:NormalMap,ptin:_NormalMap,varname:node_2711,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True;n:type:ShaderForge.SFN_ValueProperty,id:8687,x:32361,y:32960,ptovrint:False,ptlb:NormalStrength,ptin:_NormalStrength,varname:node_7639,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:82;n:type:ShaderForge.SFN_Multiply,id:7211,x:32534,y:32813,varname:node_7211,prsc:2|A-3977-RGB,B-8687-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5765,x:30585,y:33704,ptovrint:False,ptlb:VO_Strength,ptin:_VO_Strength,varname:node_7528,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_NormalVector,id:5463,x:30397,y:33628,prsc:2,pt:True;n:type:ShaderForge.SFN_Multiply,id:1481,x:30585,y:33502,varname:node_1481,prsc:2|A-9516-RGB,B-5463-OUT,C-6827-OUT;n:type:ShaderForge.SFN_LightVector,id:1758,x:27810,y:32402,varname:node_1758,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:5845,x:28317,y:32284,prsc:2,pt:True;n:type:ShaderForge.SFN_Dot,id:3769,x:28540,y:32201,varname:node_3769,prsc:2,dt:1|A-5845-OUT,B-1758-OUT;n:type:ShaderForge.SFN_ViewReflectionVector,id:6201,x:27810,y:32587,varname:node_6201,prsc:2;n:type:ShaderForge.SFN_Dot,id:8536,x:28003,y:32442,varname:node_8536,prsc:2,dt:1|A-1758-OUT,B-6201-OUT;n:type:ShaderForge.SFN_Slider,id:6061,x:27413,y:32734,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:node_3540,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.3263002,max:1;n:type:ShaderForge.SFN_RemapRange,id:4229,x:27767,y:32733,varname:node_4229,prsc:2,frmn:0,frmx:1,tomn:1,tomx:11|IN-6061-OUT;n:type:ShaderForge.SFN_Exp,id:2180,x:27954,y:32733,varname:node_2180,prsc:2,et:1|IN-4229-OUT;n:type:ShaderForge.SFN_Power,id:2004,x:28174,y:32481,varname:node_2004,prsc:2|VAL-8536-OUT,EXP-2180-OUT;n:type:ShaderForge.SFN_Multiply,id:2746,x:28446,y:32479,cmnt: Final Spec,varname:node_2746,prsc:2|A-2004-OUT,B-7839-RGB,C-2665-RGB,D-4655-OUT,E-7286-RGB;n:type:ShaderForge.SFN_Color,id:7839,x:28174,y:32642,ptovrint:False,ptlb:SpecColor,ptin:_SpecColor,varname:node_4084,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.7,c2:0.7,c3:0.7,c4:1;n:type:ShaderForge.SFN_Multiply,id:6572,x:28334,y:31928,varname:node_6572,prsc:2|A-5601-RGB,B-9554-RGB;n:type:ShaderForge.SFN_Color,id:9554,x:28143,y:32082,ptovrint:False,ptlb:DiffuseColor,ptin:_DiffuseColor,varname:node_7132,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.6488074,c2:0.7486175,c3:0.8490566,c4:1;n:type:ShaderForge.SFN_Multiply,id:9600,x:28743,y:32063,cmnt: Final Diffuse,varname:node_9600,prsc:2|A-6572-OUT,B-3769-OUT;n:type:ShaderForge.SFN_Add,id:9445,x:29199,y:32370,varname:node_9445,prsc:2|A-6439-OUT,B-374-OUT;n:type:ShaderForge.SFN_Multiply,id:6788,x:29621,y:32466,cmnt: Final Lighting,varname:node_6788,prsc:2|A-9941-OUT,B-7286-RGB,C-7904-OUT;n:type:ShaderForge.SFN_LightColor,id:7286,x:29436,y:32332,varname:node_7286,prsc:2;n:type:ShaderForge.SFN_LightAttenuation,id:7904,x:29436,y:32499,varname:node_7904,prsc:2;n:type:ShaderForge.SFN_Tex2d,id:2665,x:27364,y:33014,varname:node_8071,prsc:2,ntxv:0,isnm:False|TEX-3525-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:3525,x:27153,y:33014,ptovrint:False,ptlb:SpecMap,ptin:_SpecMap,varname:node_1505,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:9525,x:29021,y:32980,varname:node_7666,prsc:2,ntxv:0,isnm:False|UVIN-1391-OUT,TEX-6761-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:6761,x:28836,y:32980,ptovrint:False,ptlb:Clouds,ptin:_Clouds,varname:node_4472,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Time,id:9929,x:26677,y:33627,varname:node_9929,prsc:2;n:type:ShaderForge.SFN_TexCoord,id:6594,x:26677,y:33431,varname:node_6594,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_ValueProperty,id:4007,x:28275,y:33424,ptovrint:False,ptlb:CloudsSpeed_U,ptin:_CloudsSpeed_U,varname:node_4223,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.012;n:type:ShaderForge.SFN_ValueProperty,id:1259,x:28275,y:33501,ptovrint:False,ptlb:CloudsSpeed_V,ptin:_CloudsSpeed_V,varname:node_3250,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Append,id:5620,x:28457,y:33414,varname:node_5620,prsc:2|A-4007-OUT,B-1259-OUT;n:type:ShaderForge.SFN_Multiply,id:4126,x:28618,y:33424,varname:node_4126,prsc:2|A-5620-OUT,B-9929-T;n:type:ShaderForge.SFN_Add,id:1391,x:28792,y:33424,varname:node_1391,prsc:2|A-4126-OUT,B-1222-OUT;n:type:ShaderForge.SFN_Add,id:9941,x:29436,y:32626,varname:node_9941,prsc:2|A-9445-OUT,B-5530-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1759,x:28271,y:33637,ptovrint:False,ptlb:CloudsNoiseSpeed_U,ptin:_CloudsNoiseSpeed_U,varname:node_8261,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.005;n:type:ShaderForge.SFN_ValueProperty,id:2647,x:28271,y:33713,ptovrint:False,ptlb:CloudsNoiseSpeed_V,ptin:_CloudsNoiseSpeed_V,varname:node_1726,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.01;n:type:ShaderForge.SFN_Append,id:1640,x:28457,y:33627,varname:node_1640,prsc:2|A-1759-OUT,B-2647-OUT;n:type:ShaderForge.SFN_Multiply,id:3059,x:28618,y:33637,varname:node_3059,prsc:2|A-1640-OUT,B-9929-T;n:type:ShaderForge.SFN_Add,id:3918,x:28792,y:33637,varname:node_3918,prsc:2|A-3059-OUT,B-1222-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:7937,x:28836,y:33188,ptovrint:False,ptlb:CloudsNoise,ptin:_CloudsNoise,varname:node_8091,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:4399,x:29035,y:33188,varname:node_4320,prsc:2,ntxv:0,isnm:False|UVIN-3918-OUT,TEX-7937-TEX;n:type:ShaderForge.SFN_Multiply,id:5530,x:29561,y:33045,cmnt: Final Clouds,varname:node_5530,prsc:2|A-9525-RGB,B-7097-OUT,C-1692-OUT,D-6075-RGB;n:type:ShaderForge.SFN_ValueProperty,id:1692,x:29035,y:33350,ptovrint:False,ptlb:CloudsStrength,ptin:_CloudsStrength,varname:node_7281,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:5;n:type:ShaderForge.SFN_Color,id:6075,x:29035,y:33424,ptovrint:False,ptlb:CloudsColor,ptin:_CloudsColor,varname:node_8378,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.8574827,c2:0.919237,c3:0.9558824,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:3964,x:29035,y:33637,ptovrint:False,ptlb:CloudsNoiseStrength,ptin:_CloudsNoiseStrength,varname:node_9634,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Power,id:7097,x:29278,y:33392,varname:node_7097,prsc:2|VAL-4399-RGB,EXP-3964-OUT;n:type:ShaderForge.SFN_Lerp,id:7700,x:30821,y:32146,varname:node_7700,prsc:2|A-6280-OUT,B-5028-OUT,T-7428-OUT;n:type:ShaderForge.SFN_Tex2d,id:210,x:29965,y:32389,varname:node_7863,prsc:2,ntxv:0,isnm:False|TEX-2327-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:2327,x:29791,y:32389,ptovrint:False,ptlb:NightLights,ptin:_NightLights,varname:node_7859,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Power,id:654,x:30084,y:31778,varname:node_654,prsc:2|VAL-343-OUT,EXP-5581-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5581,x:29797,y:31858,ptovrint:False,ptlb:NightSkyTransitionValue,ptin:_NightSkyTransitionValue,varname:node_152,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:5;n:type:ShaderForge.SFN_OneMinus,id:6428,x:29779,y:32025,varname:node_6428,prsc:2|IN-3769-OUT;n:type:ShaderForge.SFN_Multiply,id:6280,x:29944,y:32728,varname:node_6280,prsc:2|A-6788-OUT,B-3845-OUT;n:type:ShaderForge.SFN_Multiply,id:685,x:30162,y:32490,varname:node_685,prsc:2|A-210-RGB,B-3597-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3597,x:29944,y:32585,ptovrint:False,ptlb:NightLightValues,ptin:_NightLightValues,varname:node_1182,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:3;n:type:ShaderForge.SFN_Add,id:583,x:30897,y:32571,varname:node_583,prsc:2|A-685-OUT,B-180-OUT;n:type:ShaderForge.SFN_Tex2d,id:3876,x:30556,y:32662,varname:node_3838,prsc:2,ntxv:0,isnm:False|TEX-8245-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:8245,x:30369,y:32662,ptovrint:False,ptlb:NightLightsEmissive,ptin:_NightLightsEmissive,varname:node_3921,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Color,id:3263,x:30369,y:32854,ptovrint:False,ptlb:LightsColor,ptin:_LightsColor,varname:node_839,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.6624746,c3:0.05882353,c4:1;n:type:ShaderForge.SFN_Multiply,id:7287,x:30556,y:32824,varname:node_7287,prsc:2|A-3876-RGB,B-3263-RGB,C-9409-OUT;n:type:ShaderForge.SFN_ValueProperty,id:9409,x:30556,y:32974,ptovrint:False,ptlb:LightsEmissiveValue,ptin:_LightsEmissiveValue,varname:node_1628,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2.5;n:type:ShaderForge.SFN_Clamp01,id:7428,x:30253,y:31778,varname:node_7428,prsc:2|IN-654-OUT;n:type:ShaderForge.SFN_Clamp01,id:343,x:29990,y:32025,varname:node_343,prsc:2|IN-6428-OUT;n:type:ShaderForge.SFN_OneMinus,id:1501,x:30369,y:33081,varname:node_1501,prsc:2|IN-5530-OUT;n:type:ShaderForge.SFN_Multiply,id:180,x:30821,y:32843,varname:node_180,prsc:2|A-7287-OUT,B-5854-OUT;n:type:ShaderForge.SFN_Power,id:8105,x:30737,y:33076,varname:node_8105,prsc:2|VAL-1501-OUT,EXP-5722-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5722,x:30566,y:33160,ptovrint:False,ptlb:DarkCloudsPower,ptin:_DarkCloudsPower,varname:node_5934,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Clamp01,id:5854,x:30948,y:33076,varname:node_5854,prsc:2|IN-8105-OUT;n:type:ShaderForge.SFN_Multiply,id:4655,x:27935,y:32976,varname:node_4655,prsc:2|A-2665-RGB,B-9703-OUT;n:type:ShaderForge.SFN_Multiply,id:5180,x:27232,y:33369,varname:node_5180,prsc:2|A-8306-OUT,B-9929-T;n:type:ShaderForge.SFN_Add,id:9957,x:27414,y:33369,varname:node_9957,prsc:2|A-5180-OUT,B-6594-UVOUT;n:type:ShaderForge.SFN_Multiply,id:8767,x:27232,y:33522,varname:node_8767,prsc:2|A-1681-OUT,B-9929-T;n:type:ShaderForge.SFN_Add,id:7176,x:27414,y:33522,varname:node_7176,prsc:2|A-8767-OUT,B-6594-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:3048,x:27836,y:33270,varname:node_768,prsc:2,ntxv:0,isnm:False|UVIN-9957-OUT,TEX-7639-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:7639,x:27662,y:33369,ptovrint:False,ptlb:WaterTexture,ptin:_WaterTexture,varname:node_3640,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:9891,x:27853,y:33536,varname:node_9215,prsc:2,ntxv:0,isnm:False|UVIN-7176-OUT,TEX-5679-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:5679,x:27662,y:33605,ptovrint:False,ptlb:WaterNoise,ptin:_WaterNoise,varname:node_6446,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Append,id:8306,x:27064,y:33369,varname:node_8306,prsc:2|A-240-OUT,B-5835-OUT;n:type:ShaderForge.SFN_Append,id:1681,x:27064,y:33522,varname:node_1681,prsc:2|A-8139-OUT,B-160-OUT;n:type:ShaderForge.SFN_ValueProperty,id:240,x:26846,y:33325,ptovrint:False,ptlb:WaterSpeed_U,ptin:_WaterSpeed_U,varname:node_3264,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:5835,x:26846,y:33419,ptovrint:False,ptlb:WaterSpeed_V,ptin:_WaterSpeed_V,varname:node_1523,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:8139,x:26858,y:33559,ptovrint:False,ptlb:WaterNoiseSpeed_U,ptin:_WaterNoiseSpeed_U,varname:node_6693,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:160,x:26858,y:33648,ptovrint:False,ptlb:WaterNoiseSpeed_V,ptin:_WaterNoiseSpeed_V,varname:node_4758,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Multiply,id:9703,x:28053,y:33270,varname:node_9703,prsc:2|A-3048-RGB,B-9891-RGB;n:type:ShaderForge.SFN_Multiply,id:374,x:28994,y:32462,varname:node_374,prsc:2|A-2746-OUT,B-9468-OUT,C-2833-OUT;n:type:ShaderForge.SFN_ValueProperty,id:9468,x:28776,y:32481,ptovrint:False,ptlb:SpecValue,ptin:_SpecValue,varname:node_5941,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1.5;n:type:ShaderForge.SFN_Tex2d,id:9516,x:29843,y:33438,varname:node_3136,prsc:2,ntxv:0,isnm:False|TEX-7985-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:7985,x:29660,y:33460,ptovrint:False,ptlb:VertexOffsetMap,ptin:_VertexOffsetMap,varname:node_7770,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Add,id:6827,x:30223,y:33400,varname:node_6827,prsc:2|A-7517-OUT,B-9516-RGB;n:type:ShaderForge.SFN_Multiply,id:241,x:30831,y:33540,varname:node_241,prsc:2|A-1481-OUT,B-5765-OUT;n:type:ShaderForge.SFN_Multiply,id:7517,x:30026,y:33316,varname:node_7517,prsc:2|A-5530-OUT,B-6915-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6915,x:29843,y:33377,ptovrint:False,ptlb:CloudsVO_Strength,ptin:_CloudsVO_Strength,varname:node_7530,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Fresnel,id:3237,x:31417,y:32536,varname:node_3237,prsc:2|EXP-3708-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3708,x:31228,y:32558,ptovrint:False,ptlb:SunFresnelExp,ptin:_SunFresnelExp,varname:node_5734,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Multiply,id:5127,x:31675,y:32640,cmnt:SunFresnel,varname:node_5127,prsc:2|A-3237-OUT,B-8536-OUT,C-7286-RGB,D-2605-OUT;n:type:ShaderForge.SFN_Add,id:7984,x:32390,y:32643,varname:node_7984,prsc:2|A-5127-OUT,B-5053-OUT;n:type:ShaderForge.SFN_Fresnel,id:9350,x:31731,y:32896,varname:node_9350,prsc:2|EXP-9233-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2605,x:31417,y:32796,ptovrint:False,ptlb:SunFresnelValue,ptin:_SunFresnelValue,varname:node_3315,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Color,id:4287,x:31731,y:33059,ptovrint:False,ptlb:AtmosphereColor,ptin:_AtmosphereColor,varname:node_3100,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.2726535,c2:0.6168529,c3:0.9044118,c4:1;n:type:ShaderForge.SFN_Multiply,id:3691,x:31944,y:33014,varname:node_3691,prsc:2|A-9350-OUT,B-4287-RGB;n:type:ShaderForge.SFN_ValueProperty,id:9233,x:31541,y:32930,ptovrint:False,ptlb:AtmosphereFresnelExp,ptin:_AtmosphereFresnelExp,varname:node_5676,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Multiply,id:5053,x:32139,y:33014,varname:node_5053,prsc:2|A-3691-OUT,B-7911-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7911,x:31944,y:33169,ptovrint:False,ptlb:AtmosphereValue,ptin:_AtmosphereValue,varname:node_7742,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1.5;n:type:ShaderForge.SFN_Tex2d,id:7353,x:29021,y:32854,varname:node_1662,prsc:2,ntxv:0,isnm:False|UVIN-8372-OUT,TEX-6761-TEX;n:type:ShaderForge.SFN_Multiply,id:8088,x:29294,y:32919,cmnt: CloudsShadow,varname:node_8088,prsc:2|A-7831-OUT,B-7353-RGB,C-7690-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7831,x:29021,y:32799,ptovrint:False,ptlb:CloudsShadowsStrength,ptin:_CloudsShadowsStrength,varname:node_9709,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1.5;n:type:ShaderForge.SFN_ValueProperty,id:6468,x:28267,y:32848,ptovrint:False,ptlb:CloudsShadowsDisplace_U,ptin:_CloudsShadowsDisplace_U,varname:node_6849,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.05;n:type:ShaderForge.SFN_ValueProperty,id:7663,x:28267,y:32922,ptovrint:False,ptlb:CloudsShadowsDisplace_V,ptin:_CloudsShadowsDisplace_V,varname:node_7849,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Append,id:6653,x:28448,y:32830,varname:node_6653,prsc:2|A-6468-OUT,B-7663-OUT;n:type:ShaderForge.SFN_Add,id:8372,x:28836,y:32825,varname:node_8372,prsc:2|A-6653-OUT,B-1391-OUT;n:type:ShaderForge.SFN_Clamp01,id:7690,x:29458,y:33392,varname:node_7690,prsc:2|IN-7097-OUT;n:type:ShaderForge.SFN_Lerp,id:6439,x:28992,y:32257,varname:node_6439,prsc:2|A-9600-OUT,B-431-RGB,T-8088-OUT;n:type:ShaderForge.SFN_Color,id:431,x:28732,y:32261,ptovrint:False,ptlb:CloudShadowsColor,ptin:_CloudShadowsColor,varname:node_7816,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.3088235,c2:0.3088235,c3:0.3088235,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:3845,x:29619,y:32799,ptovrint:False,ptlb:VisibleEarthValue,ptin:_VisibleEarthValue,varname:node_7753,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1.75;n:type:ShaderForge.SFN_TexCoord,id:9612,x:28255,y:33868,varname:node_9612,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Multiply,id:8531,x:28655,y:34034,varname:node_8531,prsc:2|A-3938-R,B-2822-OUT;n:type:ShaderForge.SFN_Tex2d,id:3938,x:28446,y:33975,varname:node_5342,prsc:2,ntxv:0,isnm:False|TEX-6862-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:6862,x:28262,y:34073,ptovrint:False,ptlb:CloudsMovementUV,ptin:_CloudsMovementUV,varname:node_9384,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_ValueProperty,id:2822,x:28446,y:34144,ptovrint:False,ptlb:CloudsUVDisplacementValue,ptin:_CloudsUVDisplacementValue,varname:node_5000,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:3;n:type:ShaderForge.SFN_Add,id:1222,x:28863,y:34002,varname:node_1222,prsc:2|A-9612-UVOUT,B-8531-OUT;n:type:ShaderForge.SFN_Tex2d,id:8626,x:30631,y:32414,varname:node_4607,prsc:2,ntxv:0,isnm:False|TEX-3278-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:3278,x:30460,y:32414,ptovrint:False,ptlb:LightsNoise,ptin:_LightsNoise,varname:node_7807,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:4019,x:30278,y:32258,varname:node_4019,prsc:2|A-7877-OUT,B-9929-T;n:type:ShaderForge.SFN_Add,id:5021,x:30460,y:32258,varname:node_5021,prsc:2|A-4019-OUT,B-1306-UVOUT;n:type:ShaderForge.SFN_Append,id:7877,x:30110,y:32258,varname:node_7877,prsc:2|A-8693-OUT,B-2893-OUT;n:type:ShaderForge.SFN_TexCoord,id:1306,x:30278,y:32115,varname:node_1306,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_ValueProperty,id:8693,x:29935,y:32241,ptovrint:False,ptlb:LightsNoise_U,ptin:_LightsNoise_U,varname:node_360,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.05;n:type:ShaderForge.SFN_ValueProperty,id:2893,x:29935,y:32316,ptovrint:False,ptlb:LightsNoise_V,ptin:_LightsNoise_V,varname:node_6232,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.002;n:type:ShaderForge.SFN_Multiply,id:5028,x:31126,y:32414,varname:node_5028,prsc:2|A-5135-OUT,B-583-OUT;n:type:ShaderForge.SFN_Power,id:4207,x:30801,y:32414,varname:node_4207,prsc:2|VAL-8626-RGB,EXP-5909-OUT;n:type:ShaderForge.SFN_Clamp01,id:5135,x:30963,y:32414,varname:node_5135,prsc:2|IN-4207-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5909,x:30682,y:32571,ptovrint:False,ptlb:LightsNoisePower,ptin:_LightsNoisePower,varname:node_751,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Divide,id:2170,x:28660,y:32668,varname:node_2170,prsc:2|A-8088-OUT,B-8343-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8343,x:28479,y:32702,ptovrint:False,ptlb:SpecShadowAffectation,ptin:_SpecShadowAffectation,varname:node_8343,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Add,id:1485,x:28852,y:32631,varname:node_1485,prsc:2|A-3012-OUT,B-2170-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3012,x:28660,y:32610,ptovrint:False,ptlb:SpecShadowAdd,ptin:_SpecShadowAdd,varname:node_3012,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.25;n:type:ShaderForge.SFN_OneMinus,id:5116,x:29005,y:32631,varname:node_5116,prsc:2|IN-1485-OUT;n:type:ShaderForge.SFN_Clamp01,id:2833,x:29169,y:32631,varname:node_2833,prsc:2|IN-5116-OUT;proporder:9492-8687-3708-2605-4287-9233-7911-9550-5765-6761-1759-2647-7937-1692-6075-3964-7985-6915-6862-2822-88-6061-7839-9554-3525-2327-5581-3597-8245-3263-9409-5722-7639-5679-240-5835-8139-160-9468-7831-431-3845-3278-5909-4007-1259-6468-7663-8343-3012;pass:END;sub:END;*/

Shader "Custom/Planet2" {
    Properties {
        _NormalMap ("NormalMap", 2D) = "bump" {}
        _NormalStrength ("NormalStrength", Float ) = 82
        _SunFresnelExp ("SunFresnelExp", Float ) = 2
        _SunFresnelValue ("SunFresnelValue", Float ) = 2
        _AtmosphereColor ("AtmosphereColor", Color) = (0.2726535,0.6168529,0.9044118,1)
        _AtmosphereFresnelExp ("AtmosphereFresnelExp", Float ) = 2
        _AtmosphereValue ("AtmosphereValue", Float ) = 1.5
        _Tesselation ("Tesselation", Float ) = 1
        _VO_Strength ("VO_Strength", Float ) = 0.1
        _Clouds ("Clouds", 2D) = "white" {}
        _CloudsNoiseSpeed_U ("CloudsNoiseSpeed_U", Float ) = 0.005
        _CloudsNoiseSpeed_V ("CloudsNoiseSpeed_V", Float ) = 0.01
        _CloudsNoise ("CloudsNoise", 2D) = "white" {}
        _CloudsStrength ("CloudsStrength", Float ) = 5
        _CloudsColor ("CloudsColor", Color) = (0.8574827,0.919237,0.9558824,1)
        _CloudsNoiseStrength ("CloudsNoiseStrength", Float ) = 2
        _VertexOffsetMap ("VertexOffsetMap", 2D) = "white" {}
        _CloudsVO_Strength ("CloudsVO_Strength", Float ) = 2
        _CloudsMovementUV ("CloudsMovementUV", 2D) = "white" {}
        _CloudsUVDisplacementValue ("CloudsUVDisplacementValue", Float ) = 3
        _Diffuse ("Diffuse", 2D) = "white" {}
        _Gloss ("Gloss", Range(0, 1)) = 0.3263002
        _SpecColor ("SpecColor", Color) = (0.7,0.7,0.7,1)
        _DiffuseColor ("DiffuseColor", Color) = (0.6488074,0.7486175,0.8490566,1)
        _SpecMap ("SpecMap", 2D) = "white" {}
        _NightLights ("NightLights", 2D) = "white" {}
        _NightSkyTransitionValue ("NightSkyTransitionValue", Float ) = 5
        _NightLightValues ("NightLightValues", Float ) = 3
        _NightLightsEmissive ("NightLightsEmissive", 2D) = "white" {}
        _LightsColor ("LightsColor", Color) = (1,0.6624746,0.05882353,1)
        _LightsEmissiveValue ("LightsEmissiveValue", Float ) = 2.5
        _DarkCloudsPower ("DarkCloudsPower", Float ) = 2
        _WaterTexture ("WaterTexture", 2D) = "white" {}
        _WaterNoise ("WaterNoise", 2D) = "white" {}
        _WaterSpeed_U ("WaterSpeed_U", Float ) = 0
        _WaterSpeed_V ("WaterSpeed_V", Float ) = 0
        _WaterNoiseSpeed_U ("WaterNoiseSpeed_U", Float ) = 0
        _WaterNoiseSpeed_V ("WaterNoiseSpeed_V", Float ) = 0
        _SpecValue ("SpecValue", Float ) = 1.5
        _CloudsShadowsStrength ("CloudsShadowsStrength", Float ) = 1.5
        _CloudShadowsColor ("CloudShadowsColor", Color) = (0.3088235,0.3088235,0.3088235,1)
        _VisibleEarthValue ("VisibleEarthValue", Float ) = 1.75
        _LightsNoise ("LightsNoise", 2D) = "white" {}
        _LightsNoisePower ("LightsNoisePower", Float ) = 2
        _CloudsSpeed_U ("CloudsSpeed_U", Float ) = 0.012
        _CloudsSpeed_V ("CloudsSpeed_V", Float ) = 0
        _CloudsShadowsDisplace_U ("CloudsShadowsDisplace_U", Float ) = 0.05
        _CloudsShadowsDisplace_V ("CloudsShadowsDisplace_V", Float ) = 0
        _SpecShadowAffectation ("SpecShadowAffectation", Float ) = 2
        _SpecShadowAdd ("SpecShadowAdd", Float ) = 0.25
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma hull hull
            #pragma domain domain
            #pragma vertex tessvert
            #pragma fragment frag
            #ifndef UNITY_PASS_FORWARDBASE
            #define UNITY_PASS_FORWARDBASE
            #endif //UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "Tessellation.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 2.0
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform float _Tesselation;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform float _NormalStrength;
            uniform float _VO_Strength;
            uniform float _Gloss;
            uniform float4 _DiffuseColor;
            uniform sampler2D _SpecMap; uniform float4 _SpecMap_ST;
            uniform sampler2D _Clouds; uniform float4 _Clouds_ST;
            uniform float _CloudsSpeed_U;
            uniform float _CloudsSpeed_V;
            uniform float _CloudsNoiseSpeed_U;
            uniform float _CloudsNoiseSpeed_V;
            uniform sampler2D _CloudsNoise; uniform float4 _CloudsNoise_ST;
            uniform float _CloudsStrength;
            uniform float4 _CloudsColor;
            uniform float _CloudsNoiseStrength;
            uniform sampler2D _NightLights; uniform float4 _NightLights_ST;
            uniform float _NightSkyTransitionValue;
            uniform float _NightLightValues;
            uniform sampler2D _NightLightsEmissive; uniform float4 _NightLightsEmissive_ST;
            uniform float4 _LightsColor;
            uniform float _LightsEmissiveValue;
            uniform float _DarkCloudsPower;
            uniform sampler2D _WaterTexture; uniform float4 _WaterTexture_ST;
            uniform sampler2D _WaterNoise; uniform float4 _WaterNoise_ST;
            uniform float _WaterSpeed_U;
            uniform float _WaterSpeed_V;
            uniform float _WaterNoiseSpeed_U;
            uniform float _WaterNoiseSpeed_V;
            uniform float _SpecValue;
            uniform sampler2D _VertexOffsetMap; uniform float4 _VertexOffsetMap_ST;
            uniform float _CloudsVO_Strength;
            uniform float _SunFresnelExp;
            uniform float _SunFresnelValue;
            uniform float4 _AtmosphereColor;
            uniform float _AtmosphereFresnelExp;
            uniform float _AtmosphereValue;
            uniform float _CloudsShadowsStrength;
            uniform float _CloudsShadowsDisplace_U;
            uniform float _CloudsShadowsDisplace_V;
            uniform float4 _CloudShadowsColor;
            uniform float _VisibleEarthValue;
            uniform sampler2D _CloudsMovementUV; uniform float4 _CloudsMovementUV_ST;
            uniform float _CloudsUVDisplacementValue;
            uniform sampler2D _LightsNoise; uniform float4 _LightsNoise_ST;
            uniform float _LightsNoisePower;
            uniform float _SpecShadowAffectation;
            uniform float _SpecShadowAdd;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                UNITY_LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_LIGHTING(o, float2(0,0));
                return o;
            }
            #ifdef UNITY_CAN_COMPILE_TESSELLATION
                struct TessVertex {
                    float4 vertex : INTERNALTESSPOS;
                    float3 normal : NORMAL;
                    float4 tangent : TANGENT;
                    float2 texcoord0 : TEXCOORD0;
                };
                struct OutputPatchConstant {
                    float edge[3]         : SV_TessFactor;
                    float inside          : SV_InsideTessFactor;
                    float3 vTangent[4]    : TANGENT;
                    float2 vUV[4]         : TEXCOORD;
                    float3 vTanUCorner[4] : TANUCORNER;
                    float3 vTanVCorner[4] : TANVCORNER;
                    float4 vCWts          : TANWEIGHTS;
                };
                TessVertex tessvert (VertexInput v) {
                    TessVertex o;
                    o.vertex = v.vertex;
                    o.normal = v.normal;
                    o.tangent = v.tangent;
                    o.texcoord0 = v.texcoord0;
                    return o;
                }
                void displacement (inout VertexInput v){
                    float4 node_3136 = tex2Dlod(_VertexOffsetMap,float4(TRANSFORM_TEX(v.texcoord0, _VertexOffsetMap),0.0,0));
                    float4 node_9929 = _Time;
                    float4 node_5342 = tex2Dlod(_CloudsMovementUV,float4(TRANSFORM_TEX(v.texcoord0, _CloudsMovementUV),0.0,0));
                    float2 node_1222 = (v.texcoord0+(node_5342.r*_CloudsUVDisplacementValue));
                    float2 node_1391 = ((float2(_CloudsSpeed_U,_CloudsSpeed_V)*node_9929.g)+node_1222);
                    float4 node_7666 = tex2Dlod(_Clouds,float4(TRANSFORM_TEX(node_1391, _Clouds),0.0,0));
                    float2 node_3918 = ((float2(_CloudsNoiseSpeed_U,_CloudsNoiseSpeed_V)*node_9929.g)+node_1222);
                    float4 node_4320 = tex2Dlod(_CloudsNoise,float4(TRANSFORM_TEX(node_3918, _CloudsNoise),0.0,0));
                    float3 node_7097 = pow(node_4320.rgb,_CloudsNoiseStrength);
                    float3 node_5530 = (node_7666.rgb*node_7097*_CloudsStrength*_CloudsColor.rgb); //  Final Clouds
                    v.vertex.xyz += ((node_3136.rgb*v.normal*((node_5530*_CloudsVO_Strength)+node_3136.rgb))*_VO_Strength);
                }
                float Tessellation(TessVertex v){
                    return _Tesselation;
                }
                float4 Tessellation(TessVertex v, TessVertex v1, TessVertex v2){
                    float tv = Tessellation(v);
                    float tv1 = Tessellation(v1);
                    float tv2 = Tessellation(v2);
                    return float4( tv1+tv2, tv2+tv, tv+tv1, tv+tv1+tv2 ) / float4(2,2,2,3);
                }
                OutputPatchConstant hullconst (InputPatch<TessVertex,3> v) {
                    OutputPatchConstant o = (OutputPatchConstant)0;
                    float4 ts = Tessellation( v[0], v[1], v[2] );
                    o.edge[0] = ts.x;
                    o.edge[1] = ts.y;
                    o.edge[2] = ts.z;
                    o.inside = ts.w;
                    return o;
                }
                [domain("tri")]
                [partitioning("fractional_odd")]
                [outputtopology("triangle_cw")]
                [patchconstantfunc("hullconst")]
                [outputcontrolpoints(3)]
                TessVertex hull (InputPatch<TessVertex,3> v, uint id : SV_OutputControlPointID) {
                    return v[id];
                }
                [domain("tri")]
                VertexOutput domain (OutputPatchConstant tessFactors, const OutputPatch<TessVertex,3> vi, float3 bary : SV_DomainLocation) {
                    VertexInput v = (VertexInput)0;
                    v.vertex = vi[0].vertex*bary.x + vi[1].vertex*bary.y + vi[2].vertex*bary.z;
                    v.normal = vi[0].normal*bary.x + vi[1].normal*bary.y + vi[2].normal*bary.z;
                    v.tangent = vi[0].tangent*bary.x + vi[1].tangent*bary.y + vi[2].tangent*bary.z;
                    v.texcoord0 = vi[0].texcoord0*bary.x + vi[1].texcoord0*bary.y + vi[2].texcoord0*bary.z;
                    displacement(v);
                    VertexOutput o = vert(v);
                    return o;
                }
            #endif
            float4 frag(VertexOutput i) : COLOR {
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 node_5291 = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap)));
                float3 normalLocal = (node_5291.rgb*_NormalStrength);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                UNITY_LIGHT_ATTENUATION(attenuation, i, i.posWorld.xyz);
////// Emissive:
                float node_8536 = max(0,dot(lightDirection,viewReflectDirection));
                float3 emissive = ((pow(1.0-max(0,dot(normalDirection, viewDirection)),_SunFresnelExp)*node_8536*_LightColor0.rgb*_SunFresnelValue)+((pow(1.0-max(0,dot(normalDirection, viewDirection)),_AtmosphereFresnelExp)*_AtmosphereColor.rgb)*_AtmosphereValue));
                float4 node_4000 = tex2D(_Diffuse,TRANSFORM_TEX(i.uv0, _Diffuse));
                float node_3769 = max(0,dot(normalDirection,lightDirection));
                float4 node_9929 = _Time;
                float4 node_5342 = tex2D(_CloudsMovementUV,TRANSFORM_TEX(i.uv0, _CloudsMovementUV));
                float2 node_1222 = (i.uv0+(node_5342.r*_CloudsUVDisplacementValue));
                float2 node_1391 = ((float2(_CloudsSpeed_U,_CloudsSpeed_V)*node_9929.g)+node_1222);
                float2 node_8372 = (float2(_CloudsShadowsDisplace_U,_CloudsShadowsDisplace_V)+node_1391);
                float4 node_1662 = tex2D(_Clouds,TRANSFORM_TEX(node_8372, _Clouds));
                float2 node_3918 = ((float2(_CloudsNoiseSpeed_U,_CloudsNoiseSpeed_V)*node_9929.g)+node_1222);
                float4 node_4320 = tex2D(_CloudsNoise,TRANSFORM_TEX(node_3918, _CloudsNoise));
                float3 node_7097 = pow(node_4320.rgb,_CloudsNoiseStrength);
                float3 node_8088 = (_CloudsShadowsStrength*node_1662.rgb*saturate(node_7097)); //  CloudsShadow
                float4 node_8071 = tex2D(_SpecMap,TRANSFORM_TEX(i.uv0, _SpecMap));
                float2 node_9957 = ((float2(_WaterSpeed_U,_WaterSpeed_V)*node_9929.g)+i.uv0);
                float4 node_768 = tex2D(_WaterTexture,TRANSFORM_TEX(node_9957, _WaterTexture));
                float2 node_7176 = ((float2(_WaterNoiseSpeed_U,_WaterNoiseSpeed_V)*node_9929.g)+i.uv0);
                float4 node_9215 = tex2D(_WaterNoise,TRANSFORM_TEX(node_7176, _WaterNoise));
                float4 node_7666 = tex2D(_Clouds,TRANSFORM_TEX(node_1391, _Clouds));
                float3 node_5530 = (node_7666.rgb*node_7097*_CloudsStrength*_CloudsColor.rgb); //  Final Clouds
                float4 node_4607 = tex2D(_LightsNoise,TRANSFORM_TEX(i.uv0, _LightsNoise));
                float4 node_7863 = tex2D(_NightLights,TRANSFORM_TEX(i.uv0, _NightLights));
                float4 node_3838 = tex2D(_NightLightsEmissive,TRANSFORM_TEX(i.uv0, _NightLightsEmissive));
                float3 finalColor = emissive + lerp(((((lerp(((node_4000.rgb*_DiffuseColor.rgb)*node_3769),_CloudShadowsColor.rgb,node_8088)+((pow(node_8536,exp2((_Gloss*10.0+1.0)))*_SpecColor.rgb*node_8071.rgb*(node_8071.rgb*(node_768.rgb*node_9215.rgb))*_LightColor0.rgb)*_SpecValue*saturate((1.0 - (_SpecShadowAdd+(node_8088/_SpecShadowAffectation))))))+node_5530)*_LightColor0.rgb*attenuation)*_VisibleEarthValue),(saturate(pow(node_4607.rgb,_LightsNoisePower))*((node_7863.rgb*_NightLightValues)+((node_3838.rgb*_LightsColor.rgb*_LightsEmissiveValue)*saturate(pow((1.0 - node_5530),_DarkCloudsPower))))),saturate(pow(saturate((1.0 - node_3769)),_NightSkyTransitionValue)));
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma hull hull
            #pragma domain domain
            #pragma vertex tessvert
            #pragma fragment frag
            #ifndef UNITY_PASS_FORWARDADD
            #define UNITY_PASS_FORWARDADD
            #endif //UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "Tessellation.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 2.0
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform float _Tesselation;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform float _NormalStrength;
            uniform float _VO_Strength;
            uniform float _Gloss;
            uniform float4 _DiffuseColor;
            uniform sampler2D _SpecMap; uniform float4 _SpecMap_ST;
            uniform sampler2D _Clouds; uniform float4 _Clouds_ST;
            uniform float _CloudsSpeed_U;
            uniform float _CloudsSpeed_V;
            uniform float _CloudsNoiseSpeed_U;
            uniform float _CloudsNoiseSpeed_V;
            uniform sampler2D _CloudsNoise; uniform float4 _CloudsNoise_ST;
            uniform float _CloudsStrength;
            uniform float4 _CloudsColor;
            uniform float _CloudsNoiseStrength;
            uniform sampler2D _NightLights; uniform float4 _NightLights_ST;
            uniform float _NightSkyTransitionValue;
            uniform float _NightLightValues;
            uniform sampler2D _NightLightsEmissive; uniform float4 _NightLightsEmissive_ST;
            uniform float4 _LightsColor;
            uniform float _LightsEmissiveValue;
            uniform float _DarkCloudsPower;
            uniform sampler2D _WaterTexture; uniform float4 _WaterTexture_ST;
            uniform sampler2D _WaterNoise; uniform float4 _WaterNoise_ST;
            uniform float _WaterSpeed_U;
            uniform float _WaterSpeed_V;
            uniform float _WaterNoiseSpeed_U;
            uniform float _WaterNoiseSpeed_V;
            uniform float _SpecValue;
            uniform sampler2D _VertexOffsetMap; uniform float4 _VertexOffsetMap_ST;
            uniform float _CloudsVO_Strength;
            uniform float _SunFresnelExp;
            uniform float _SunFresnelValue;
            uniform float4 _AtmosphereColor;
            uniform float _AtmosphereFresnelExp;
            uniform float _AtmosphereValue;
            uniform float _CloudsShadowsStrength;
            uniform float _CloudsShadowsDisplace_U;
            uniform float _CloudsShadowsDisplace_V;
            uniform float4 _CloudShadowsColor;
            uniform float _VisibleEarthValue;
            uniform sampler2D _CloudsMovementUV; uniform float4 _CloudsMovementUV_ST;
            uniform float _CloudsUVDisplacementValue;
            uniform sampler2D _LightsNoise; uniform float4 _LightsNoise_ST;
            uniform float _LightsNoisePower;
            uniform float _SpecShadowAffectation;
            uniform float _SpecShadowAdd;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                UNITY_LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_LIGHTING(o, float2(0,0));
                return o;
            }
            #ifdef UNITY_CAN_COMPILE_TESSELLATION
                struct TessVertex {
                    float4 vertex : INTERNALTESSPOS;
                    float3 normal : NORMAL;
                    float4 tangent : TANGENT;
                    float2 texcoord0 : TEXCOORD0;
                };
                struct OutputPatchConstant {
                    float edge[3]         : SV_TessFactor;
                    float inside          : SV_InsideTessFactor;
                    float3 vTangent[4]    : TANGENT;
                    float2 vUV[4]         : TEXCOORD;
                    float3 vTanUCorner[4] : TANUCORNER;
                    float3 vTanVCorner[4] : TANVCORNER;
                    float4 vCWts          : TANWEIGHTS;
                };
                TessVertex tessvert (VertexInput v) {
                    TessVertex o;
                    o.vertex = v.vertex;
                    o.normal = v.normal;
                    o.tangent = v.tangent;
                    o.texcoord0 = v.texcoord0;
                    return o;
                }
                void displacement (inout VertexInput v){
                    float4 node_3136 = tex2Dlod(_VertexOffsetMap,float4(TRANSFORM_TEX(v.texcoord0, _VertexOffsetMap),0.0,0));
                    float4 node_9929 = _Time;
                    float4 node_5342 = tex2Dlod(_CloudsMovementUV,float4(TRANSFORM_TEX(v.texcoord0, _CloudsMovementUV),0.0,0));
                    float2 node_1222 = (v.texcoord0+(node_5342.r*_CloudsUVDisplacementValue));
                    float2 node_1391 = ((float2(_CloudsSpeed_U,_CloudsSpeed_V)*node_9929.g)+node_1222);
                    float4 node_7666 = tex2Dlod(_Clouds,float4(TRANSFORM_TEX(node_1391, _Clouds),0.0,0));
                    float2 node_3918 = ((float2(_CloudsNoiseSpeed_U,_CloudsNoiseSpeed_V)*node_9929.g)+node_1222);
                    float4 node_4320 = tex2Dlod(_CloudsNoise,float4(TRANSFORM_TEX(node_3918, _CloudsNoise),0.0,0));
                    float3 node_7097 = pow(node_4320.rgb,_CloudsNoiseStrength);
                    float3 node_5530 = (node_7666.rgb*node_7097*_CloudsStrength*_CloudsColor.rgb); //  Final Clouds
                    v.vertex.xyz += ((node_3136.rgb*v.normal*((node_5530*_CloudsVO_Strength)+node_3136.rgb))*_VO_Strength);
                }
                float Tessellation(TessVertex v){
                    return _Tesselation;
                }
                float4 Tessellation(TessVertex v, TessVertex v1, TessVertex v2){
                    float tv = Tessellation(v);
                    float tv1 = Tessellation(v1);
                    float tv2 = Tessellation(v2);
                    return float4( tv1+tv2, tv2+tv, tv+tv1, tv+tv1+tv2 ) / float4(2,2,2,3);
                }
                OutputPatchConstant hullconst (InputPatch<TessVertex,3> v) {
                    OutputPatchConstant o = (OutputPatchConstant)0;
                    float4 ts = Tessellation( v[0], v[1], v[2] );
                    o.edge[0] = ts.x;
                    o.edge[1] = ts.y;
                    o.edge[2] = ts.z;
                    o.inside = ts.w;
                    return o;
                }
                [domain("tri")]
                [partitioning("fractional_odd")]
                [outputtopology("triangle_cw")]
                [patchconstantfunc("hullconst")]
                [outputcontrolpoints(3)]
                TessVertex hull (InputPatch<TessVertex,3> v, uint id : SV_OutputControlPointID) {
                    return v[id];
                }
                [domain("tri")]
                VertexOutput domain (OutputPatchConstant tessFactors, const OutputPatch<TessVertex,3> vi, float3 bary : SV_DomainLocation) {
                    VertexInput v = (VertexInput)0;
                    v.vertex = vi[0].vertex*bary.x + vi[1].vertex*bary.y + vi[2].vertex*bary.z;
                    v.normal = vi[0].normal*bary.x + vi[1].normal*bary.y + vi[2].normal*bary.z;
                    v.tangent = vi[0].tangent*bary.x + vi[1].tangent*bary.y + vi[2].tangent*bary.z;
                    v.texcoord0 = vi[0].texcoord0*bary.x + vi[1].texcoord0*bary.y + vi[2].texcoord0*bary.z;
                    displacement(v);
                    VertexOutput o = vert(v);
                    return o;
                }
            #endif
            float4 frag(VertexOutput i) : COLOR {
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 node_5291 = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap)));
                float3 normalLocal = (node_5291.rgb*_NormalStrength);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                UNITY_LIGHT_ATTENUATION(attenuation, i, i.posWorld.xyz);
                float4 node_4000 = tex2D(_Diffuse,TRANSFORM_TEX(i.uv0, _Diffuse));
                float node_3769 = max(0,dot(normalDirection,lightDirection));
                float4 node_9929 = _Time;
                float4 node_5342 = tex2D(_CloudsMovementUV,TRANSFORM_TEX(i.uv0, _CloudsMovementUV));
                float2 node_1222 = (i.uv0+(node_5342.r*_CloudsUVDisplacementValue));
                float2 node_1391 = ((float2(_CloudsSpeed_U,_CloudsSpeed_V)*node_9929.g)+node_1222);
                float2 node_8372 = (float2(_CloudsShadowsDisplace_U,_CloudsShadowsDisplace_V)+node_1391);
                float4 node_1662 = tex2D(_Clouds,TRANSFORM_TEX(node_8372, _Clouds));
                float2 node_3918 = ((float2(_CloudsNoiseSpeed_U,_CloudsNoiseSpeed_V)*node_9929.g)+node_1222);
                float4 node_4320 = tex2D(_CloudsNoise,TRANSFORM_TEX(node_3918, _CloudsNoise));
                float3 node_7097 = pow(node_4320.rgb,_CloudsNoiseStrength);
                float3 node_8088 = (_CloudsShadowsStrength*node_1662.rgb*saturate(node_7097)); //  CloudsShadow
                float node_8536 = max(0,dot(lightDirection,viewReflectDirection));
                float4 node_8071 = tex2D(_SpecMap,TRANSFORM_TEX(i.uv0, _SpecMap));
                float2 node_9957 = ((float2(_WaterSpeed_U,_WaterSpeed_V)*node_9929.g)+i.uv0);
                float4 node_768 = tex2D(_WaterTexture,TRANSFORM_TEX(node_9957, _WaterTexture));
                float2 node_7176 = ((float2(_WaterNoiseSpeed_U,_WaterNoiseSpeed_V)*node_9929.g)+i.uv0);
                float4 node_9215 = tex2D(_WaterNoise,TRANSFORM_TEX(node_7176, _WaterNoise));
                float4 node_7666 = tex2D(_Clouds,TRANSFORM_TEX(node_1391, _Clouds));
                float3 node_5530 = (node_7666.rgb*node_7097*_CloudsStrength*_CloudsColor.rgb); //  Final Clouds
                float4 node_4607 = tex2D(_LightsNoise,TRANSFORM_TEX(i.uv0, _LightsNoise));
                float4 node_7863 = tex2D(_NightLights,TRANSFORM_TEX(i.uv0, _NightLights));
                float4 node_3838 = tex2D(_NightLightsEmissive,TRANSFORM_TEX(i.uv0, _NightLightsEmissive));
                float3 finalColor = lerp(((((lerp(((node_4000.rgb*_DiffuseColor.rgb)*node_3769),_CloudShadowsColor.rgb,node_8088)+((pow(node_8536,exp2((_Gloss*10.0+1.0)))*_SpecColor.rgb*node_8071.rgb*(node_8071.rgb*(node_768.rgb*node_9215.rgb))*_LightColor0.rgb)*_SpecValue*saturate((1.0 - (_SpecShadowAdd+(node_8088/_SpecShadowAffectation))))))+node_5530)*_LightColor0.rgb*attenuation)*_VisibleEarthValue),(saturate(pow(node_4607.rgb,_LightsNoisePower))*((node_7863.rgb*_NightLightValues)+((node_3838.rgb*_LightsColor.rgb*_LightsEmissiveValue)*saturate(pow((1.0 - node_5530),_DarkCloudsPower))))),saturate(pow(saturate((1.0 - node_3769)),_NightSkyTransitionValue)));
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "Standard"
    CustomEditor "ShaderForgeMaterialInspector"
}
