// Shader created with Shader Forge v1.42 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Enhanced by Antoine Guillon / Arkham Development - http://www.arkham-development.com/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.42;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:497,x:34001,y:33157,varname:node_497,prsc:2|normal-8340-RGB,emission-5783-OUT,custl-9857-OUT;n:type:ShaderForge.SFN_NormalVector,id:997,x:32422,y:32843,prsc:2,pt:True;n:type:ShaderForge.SFN_LightVector,id:541,x:32422,y:33087,varname:node_541,prsc:2;n:type:ShaderForge.SFN_Dot,id:1459,x:32641,y:32935,varname:node_1459,prsc:2,dt:1|A-997-OUT,B-541-OUT;n:type:ShaderForge.SFN_Set,id:7156,x:32813,y:32917,varname:lightderection,prsc:2|IN-1459-OUT;n:type:ShaderForge.SFN_Get,id:6665,x:32431,y:33607,varname:node_6665,prsc:2|IN-7156-OUT;n:type:ShaderForge.SFN_LightAttenuation,id:7194,x:32445,y:33309,varname:node_7194,prsc:2;n:type:ShaderForge.SFN_Multiply,id:3728,x:32654,y:33358,varname:node_3728,prsc:2|A-7194-OUT,B-4246-RGB;n:type:ShaderForge.SFN_LightColor,id:4246,x:32419,y:33455,varname:node_4246,prsc:2;n:type:ShaderForge.SFN_Set,id:1963,x:32814,y:33633,varname:lightdata,prsc:2|IN-4849-OUT;n:type:ShaderForge.SFN_Set,id:7550,x:32834,y:33378,varname:lightcolor,prsc:2|IN-3728-OUT;n:type:ShaderForge.SFN_Get,id:15,x:32424,y:33701,varname:node_15,prsc:2|IN-7550-OUT;n:type:ShaderForge.SFN_Multiply,id:4849,x:32630,y:33618,varname:node_4849,prsc:2|A-6665-OUT,B-15-OUT;n:type:ShaderForge.SFN_Tex2d,id:2261,x:32435,y:33857,ptovrint:False,ptlb:TexturaDifusse,ptin:_TexturaDifusse,varname:node_2261,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:a566a8da5216909fcb698cc206adcb5a,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Color,id:7010,x:32435,y:34072,ptovrint:False,ptlb:node_7010,ptin:_node_7010,varname:node_7010,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.8396226,c2:0.9436036,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:1689,x:32630,y:33981,varname:node_1689,prsc:2|A-2261-RGB,B-7010-RGB;n:type:ShaderForge.SFN_Set,id:8049,x:32864,y:34017,varname:diffusedata,prsc:2|IN-1689-OUT;n:type:ShaderForge.SFN_Get,id:4412,x:33130,y:33431,varname:node_4412,prsc:2|IN-1963-OUT;n:type:ShaderForge.SFN_Get,id:9611,x:33130,y:33522,varname:node_9611,prsc:2|IN-8049-OUT;n:type:ShaderForge.SFN_Multiply,id:2386,x:33315,y:33448,varname:node_2386,prsc:2|A-4412-OUT,B-9611-OUT;n:type:ShaderForge.SFN_AmbientLight,id:5173,x:33042,y:33166,varname:node_5173,prsc:2;n:type:ShaderForge.SFN_Get,id:1468,x:33096,y:33312,varname:node_1468,prsc:2|IN-8049-OUT;n:type:ShaderForge.SFN_Multiply,id:7056,x:33256,y:33195,varname:node_7056,prsc:2|A-5173-RGB,B-1468-OUT;n:type:ShaderForge.SFN_Set,id:71,x:33426,y:33137,varname:ambientdata,prsc:2|IN-7056-OUT;n:type:ShaderForge.SFN_Tex2d,id:7998,x:33002,y:33798,ptovrint:False,ptlb:node_7998,ptin:_node_7998,varname:node_7998,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:5155562c48e13006295788924a94d7c1,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Set,id:8079,x:33475,y:33825,varname:Emissive,prsc:2|IN-647-OUT;n:type:ShaderForge.SFN_Get,id:5783,x:33771,y:33247,varname:node_5783,prsc:2|IN-8079-OUT;n:type:ShaderForge.SFN_HalfVector,id:9564,x:32451,y:32370,varname:node_9564,prsc:2;n:type:ShaderForge.SFN_Dot,id:7900,x:32623,y:32438,varname:node_7900,prsc:2,dt:1|A-9564-OUT,B-9239-OUT;n:type:ShaderForge.SFN_Power,id:8141,x:32849,y:32378,varname:node_8141,prsc:2|VAL-7900-OUT,EXP-6309-OUT;n:type:ShaderForge.SFN_Exp,id:6309,x:32746,y:32220,varname:node_6309,prsc:2,et:0|IN-4950-OUT;n:type:ShaderForge.SFN_Slider,id:4950,x:32415,y:32220,ptovrint:False,ptlb:node_4950,ptin:_node_4950,varname:node_4950,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:3.128823,max:10;n:type:ShaderForge.SFN_Multiply,id:2116,x:33101,y:32378,varname:node_2116,prsc:2|A-5012-OUT,B-8141-OUT,C-1421-RGB;n:type:ShaderForge.SFN_Slider,id:5012,x:32924,y:32269,ptovrint:False,ptlb:node_5012,ptin:_node_5012,varname:node_5012,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:2;n:type:ShaderForge.SFN_Color,id:1421,x:32941,y:32535,ptovrint:False,ptlb:node_1421,ptin:_node_1421,varname:node_1421,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:421,x:33311,y:32377,varname:node_421,prsc:2|A-2116-OUT,B-7227-OUT;n:type:ShaderForge.SFN_Get,id:7227,x:33172,y:32535,varname:node_7227,prsc:2|IN-1963-OUT;n:type:ShaderForge.SFN_Set,id:551,x:33464,y:32413,varname:speculardata,prsc:2|IN-421-OUT;n:type:ShaderForge.SFN_Get,id:8984,x:33465,y:33568,varname:node_8984,prsc:2|IN-551-OUT;n:type:ShaderForge.SFN_NormalVector,id:9239,x:32410,y:32562,prsc:2,pt:True;n:type:ShaderForge.SFN_Set,id:9916,x:33487,y:33458,varname:Diffuseshader,prsc:2|IN-2386-OUT;n:type:ShaderForge.SFN_Add,id:4170,x:33656,y:33553,varname:node_4170,prsc:2|A-8984-OUT,B-7953-OUT;n:type:ShaderForge.SFN_Get,id:7953,x:33465,y:33644,varname:node_7953,prsc:2|IN-9916-OUT;n:type:ShaderForge.SFN_Set,id:2085,x:33789,y:33737,varname:Specularshader,prsc:2|IN-4170-OUT;n:type:ShaderForge.SFN_Get,id:9857,x:33822,y:33397,varname:node_9857,prsc:2|IN-2085-OUT;n:type:ShaderForge.SFN_Tex2d,id:8340,x:33731,y:33030,ptovrint:False,ptlb:node_8340,ptin:_node_8340,varname:node_8340,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:73ac70f4d3853f2e4a3756f833875d4c,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Multiply,id:647,x:33250,y:33720,varname:node_647,prsc:2|A-7258-RGB,B-7998-RGB,C-1495-OUT;n:type:ShaderForge.SFN_Slider,id:1495,x:33129,y:34012,ptovrint:False,ptlb:node_1495,ptin:_node_1495,varname:node_1495,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.4456667,max:1;n:type:ShaderForge.SFN_Color,id:7258,x:33018,y:33609,ptovrint:False,ptlb:node_7258,ptin:_node_7258,varname:node_7258,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.4482912,c2:0.6021854,c3:0.6509434,c4:0.3921569;proporder:2261-7010-7998-4950-5012-1421-8340-1495-7258;pass:END;sub:END;*/

Shader "Unlit/Prueba" {
    Properties {
        _TexturaDifusse ("TexturaDifusse", 2D) = "white" {}
        _node_7010 ("node_7010", Color) = (0.8396226,0.9436036,1,1)
        _node_7998 ("node_7998", 2D) = "black" {}
        _node_4950 ("node_4950", Range(0, 10)) = 3.128823
        _node_5012 ("node_5012", Range(0, 2)) = 1
        _node_1421 ("node_1421", Color) = (0.5,0.5,0.5,1)
        _node_8340 ("node_8340", 2D) = "bump" {}
        _node_1495 ("node_1495", Range(0, 1)) = 0.4456667
        _node_7258 ("node_7258", Color) = (0.4482912,0.6021854,0.6509434,0.3921569)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 100
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #ifndef UNITY_PASS_FORWARDBASE
            #define UNITY_PASS_FORWARDBASE
            #endif //UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles vulkan 
            #pragma target 3.0
            uniform sampler2D _TexturaDifusse; uniform float4 _TexturaDifusse_ST;
            uniform float4 _node_7010;
            uniform sampler2D _node_7998; uniform float4 _node_7998_ST;
            uniform float _node_4950;
            uniform float _node_5012;
            uniform float4 _node_1421;
            uniform sampler2D _node_8340; uniform float4 _node_8340_ST;
            uniform float _node_1495;
            uniform float4 _node_7258;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                UNITY_LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                UNITY_TRANSFER_LIGHTING(o, float2(0,0));
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _node_8340_var = UnpackNormal(tex2D(_node_8340,TRANSFORM_TEX(i.uv0, _node_8340)));
                float3 normalLocal = _node_8340_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                UNITY_LIGHT_ATTENUATION(attenuation, i, i.posWorld.xyz);
////// Emissive:
                float4 _node_7998_var = tex2D(_node_7998,TRANSFORM_TEX(i.uv0, _node_7998));
                float3 Emissive = (_node_7258.rgb*_node_7998_var.rgb*_node_1495);
                float3 emissive = Emissive;
                float lightderection = max(0,dot(normalDirection,lightDirection));
                float3 lightcolor = (attenuation*_LightColor0.rgb);
                float3 lightdata = (lightderection*lightcolor);
                float3 speculardata = ((_node_5012*pow(max(0,dot(halfDirection,normalDirection)),exp(_node_4950))*_node_1421.rgb)*lightdata);
                float4 _TexturaDifusse_var = tex2D(_TexturaDifusse,TRANSFORM_TEX(i.uv0, _TexturaDifusse));
                float3 diffusedata = (_TexturaDifusse_var.rgb*_node_7010.rgb);
                float3 Diffuseshader = (lightdata*diffusedata);
                float3 Specularshader = (speculardata+Diffuseshader);
                float3 finalColor = emissive + Specularshader;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #ifndef UNITY_PASS_FORWARDADD
            #define UNITY_PASS_FORWARDADD
            #endif //UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles vulkan 
            #pragma target 3.0
            uniform sampler2D _TexturaDifusse; uniform float4 _TexturaDifusse_ST;
            uniform float4 _node_7010;
            uniform sampler2D _node_7998; uniform float4 _node_7998_ST;
            uniform float _node_4950;
            uniform float _node_5012;
            uniform float4 _node_1421;
            uniform sampler2D _node_8340; uniform float4 _node_8340_ST;
            uniform float _node_1495;
            uniform float4 _node_7258;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                UNITY_LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                UNITY_TRANSFER_LIGHTING(o, float2(0,0));
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _node_8340_var = UnpackNormal(tex2D(_node_8340,TRANSFORM_TEX(i.uv0, _node_8340)));
                float3 normalLocal = _node_8340_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                UNITY_LIGHT_ATTENUATION(attenuation, i, i.posWorld.xyz);
                float lightderection = max(0,dot(normalDirection,lightDirection));
                float3 lightcolor = (attenuation*_LightColor0.rgb);
                float3 lightdata = (lightderection*lightcolor);
                float3 speculardata = ((_node_5012*pow(max(0,dot(halfDirection,normalDirection)),exp(_node_4950))*_node_1421.rgb)*lightdata);
                float4 _TexturaDifusse_var = tex2D(_TexturaDifusse,TRANSFORM_TEX(i.uv0, _TexturaDifusse));
                float3 diffusedata = (_TexturaDifusse_var.rgb*_node_7010.rgb);
                float3 Diffuseshader = (lightdata*diffusedata);
                float3 Specularshader = (speculardata+Diffuseshader);
                float3 finalColor = Specularshader;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
